package ru.shem.model;

import ru.shem.data.School;
import ru.shem.model.datareavers.GroupsFactory;
import ru.shem.model.datareavers.StudentsFactory;
import ru.shem.model.datareavers.SubjectsFactory;
import ru.shem.model.datareavers.TeachersFactory;

public class SchoolControls {

    private static School school;

    public SchoolControls() {
        StudentsFactory.getInstance();
        TeachersFactory.getInstance();
        school = new School(GroupsFactory.getInstance().getList(),
                    SubjectsFactory.getInstance().getList());
    }

    public School getStaticSchool() {
        return SchoolControls.school;
    }

    public void save() {
        SchoolControls.school.getGroupList().forEach(group -> {
            String groupFilePath = "./resources/" + group.getGroupCode() + ".group";

            GroupsFactory.getInstance().saveDataToFile(groupFilePath, group);
        });
    }
}
