package ru.shem.model.datareavers;

import ru.shem.data.structures.Subject;

import java.util.LinkedList;

public class SubjectsFactory extends SchoolFactory {

    protected static LinkedList<Subject> subjectsList;

    private final String DATA_PATH = "./resources/raw/subjects.txt";

    private static SubjectsFactory instance = new SubjectsFactory();

    private SubjectsFactory() {
        String allSubjects = reaveRawDataFromFile(DATA_PATH);

        String[] lines = allSubjects.split("[\r\n]");
        LinkedList<Subject> subjectsList = new LinkedList<>();

        for(String line : lines) {
            Subject subject = new Subject(line.trim());
            subjectsList.add(subject);
        }

        SubjectsFactory.subjectsList = subjectsList;
    }

    public static SubjectsFactory getInstance() {
        return instance;
    }

    @Override
    public LinkedList<Subject> getList() {
        return SubjectsFactory.subjectsList;
    }
}
