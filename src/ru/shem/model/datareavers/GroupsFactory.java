package ru.shem.model.datareavers;

import com.oracle.tools.packager.Log;
import ru.shem.data.souls.Student;
import ru.shem.data.souls.Teacher;
import ru.shem.data.structures.Group;

import java.util.LinkedList;
import java.util.List;

public class GroupsFactory extends SchoolFactory {

    protected static LinkedList<Group> groupsList;

    private static GroupsFactory instance = new GroupsFactory();

    private GroupsFactory() {
        LinkedList<String> pathList = new LinkedList<>();
        for(int i = 1; i <= 10; ++i) {
            pathList.add("./resources/G-" + i + ".group");
        }
        LinkedList groupSavedData = reaveObjectsFromFile(pathList);
        pathList.clear();

        if((groupSavedData != null) && (!groupSavedData.isEmpty())) {
            Log.debug("loading");
            GroupsFactory.groupsList = groupSavedData;
        } else {
            Log.debug("creating");
            createGroups();
        }
    }

    private void createGroups() {
        LinkedList<Group> groupsList = new LinkedList<>();

        LinkedList<Student> studentsList = StudentsFactory.studentsList;
        LinkedList<Teacher> teachersList = TeachersFactory.teachersList;

        int groupNormalCount = 20;
        int groupsCount = studentsList.size() / groupNormalCount;

        if(groupsCount != 0) {
            for(int i = 0; i < groupsCount; i++) {
                List<Student> subList = studentsList.subList(i * groupNormalCount, (i + 1) * groupNormalCount);
                LinkedList<Student> currentGroup = new LinkedList<>(subList);
                Teacher classroomTeacher = teachersList.get(i);

                Group group = new Group(classroomTeacher, currentGroup);

                groupsList.add(group);
            }
        }
        GroupsFactory.groupsList = groupsList;
    }

    public static GroupsFactory getInstance() {
        return instance;
    }

    @Override
    public LinkedList<Group> getList() {
        return GroupsFactory.groupsList;
    }
}
