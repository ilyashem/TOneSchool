package ru.shem.model.datareavers;

import java.io.*;
import java.util.LinkedList;

public abstract class SchoolFactory<T> {

    public abstract LinkedList getList();

    String reaveRawDataFromFile(String fileLocation) {
        StringBuilder wholeFile = new StringBuilder();

        File dataFile = new File(fileLocation);
        FileInputStream stream = null;

        if(!dataFile.exists()) {
            return null;
        }

        try {
            stream = new FileInputStream(dataFile);

            Boolean eof = false;
            while (!eof) {
                int byteValue = stream.read();
                wholeFile.append((char) byteValue);
                if(byteValue == -1) {
                    eof = true;
                }
            }
        } catch (IOException e) {
            System.out.println("Could not read file: " + e.toString());
        } finally {
            if(stream != null) {
                try {
                    stream.close();
                } catch(IOException e) {
                    System.out.println("Could not close file: " + e.toString());
                }
            }
        }
        return wholeFile.toString();
    }

    LinkedList<T> reaveObjectsFromFile(LinkedList<String> pathArray) {
        LinkedList<T> dataList = new LinkedList<>();

        File dataFile;
        for(String path : pathArray) {
            dataFile = new File(path);

            if(!dataFile.exists()) {
                break;
            }
            dataList.add(accessReaverToObject(dataFile));
        }

        return dataList;
    }

    private T accessReaverToObject(File dataFile) {
        FileInputStream stream = null;
        ObjectInputStream serial = null;
        T dataObject = null;

        try {
            stream = new FileInputStream(dataFile);
            serial = new ObjectInputStream(stream);

            dataObject = (T) serial.readObject();
        } catch (IOException e) {
            System.out.println("Could not read file: " + e.toString());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            if((serial != null) && (stream != null)) {
                try {
                    serial.close();
                    stream.close();
                } catch(IOException e) {
                    System.out.println("Could not close file: " + e.toString());
                }
            }
        }
        return dataObject;
    }

    public void saveDataToFile(String fileLocation, String data) {
        FileOutputStream stream = null;

        try {
            stream = new FileOutputStream(fileLocation);

            stream.write(data.getBytes());
            stream.flush();
        } catch(IOException e) {
            System.out.println("Could not read file: " + e.toString());
        } finally {
            if(stream != null) {
                try {
                    stream.close();
                } catch(IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void saveDataToFile(String fileLocation, Object data) {
        FileOutputStream stream = null;
        ObjectOutputStream serial = null;

        try {
            stream = new FileOutputStream(fileLocation);
            serial = new ObjectOutputStream(stream);

            serial.writeObject(data);
            serial.flush();
        } catch(IOException e) {
            System.out.println("Could not read file: " + e.toString());
        } finally {
            if((serial != null) && (stream != null)) {
                try {
                    serial.close();
                    stream.close();
                } catch(IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
