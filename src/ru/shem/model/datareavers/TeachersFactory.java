package ru.shem.model.datareavers;

import ru.shem.data.souls.Teacher;

import java.util.LinkedList;

public class TeachersFactory extends SchoolFactory {

    protected static LinkedList<Teacher> teachersList;

    private final String DATA_PATH = "./resources/raw/teachers.txt";

    private static TeachersFactory instance = new TeachersFactory();

    private TeachersFactory() {
        String allTeachers = reaveRawDataFromFile(DATA_PATH);

        String[] lines = allTeachers.split("[\r\n]");
        LinkedList<Teacher> teachersList = new LinkedList<>();

        for(int i = 0; i < SubjectsFactory.subjectsList.size(); i++) {
            String line = lines[i];
            String[] fullName = line.split("\\s", 2);

            Teacher teacher = new Teacher(fullName[0].trim(), fullName[1].trim());
            teachersList.add(teacher);
            SubjectsFactory.subjectsList.get(i).setTeacher(teacher);
        }

        TeachersFactory.teachersList = teachersList;
    }

    public static TeachersFactory getInstance() {
        return instance;
    }

    @Override
    public LinkedList<Teacher> getList() {
        return TeachersFactory.teachersList;
    }
}
