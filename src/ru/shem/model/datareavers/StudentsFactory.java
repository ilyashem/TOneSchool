package ru.shem.model.datareavers;

import ru.shem.data.souls.Student;
import ru.shem.util.NameGenerator;

import java.util.LinkedList;

public class StudentsFactory extends SchoolFactory {

    protected static LinkedList<Student> studentsList;

    private final String DATA_PATH = "./resources/raw/children.txt";

    private static StudentsFactory instance = new StudentsFactory();

    private static final int STUDENTS_COUNT = 100;

    private StudentsFactory() {
        String allStudents = reaveRawDataFromFile(DATA_PATH);
        LinkedList<Student> studentsList = new LinkedList<>();

        String[] lines;
        StringBuilder allNamesForSave = new StringBuilder();

        if((allStudents == null) || (allStudents.isEmpty())) {
            lines = NameGenerator.generateNamesArray(STUDENTS_COUNT);
            for(String fullName : lines) {
                allNamesForSave.append(fullName).append('\n');
            }
            saveDataToFile(DATA_PATH, allNamesForSave.toString().trim());
        } else {
            lines = allStudents.split("[\r\n]");
        }

        for(String line : lines) {
            String[] fullName = line.split("\\s", 2);
            Student student = new Student(fullName[0].trim(), fullName[1].trim());
            studentsList.add(student);
        }
        StudentsFactory.studentsList = studentsList;
    }

    public static StudentsFactory getInstance() {
        return instance;
    }

    @Override
    public LinkedList<Student> getList() {
        return StudentsFactory.studentsList;
    }
}
