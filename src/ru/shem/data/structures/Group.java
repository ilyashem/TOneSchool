package ru.shem.data.structures;

import ru.shem.data.souls.Student;
import ru.shem.data.souls.Teacher;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;
import java.util.Formatter;
import java.util.LinkedList;
import java.util.function.Predicate;

public class Group implements Serializable {

    private static int codes = 0;

    private String groupCode;

    private Teacher classroomTeacher;

    private LinkedList<Student> studentsList;

    private Journal classJournal;

    public Group(Teacher classroomTeacher, LinkedList<Student> studentsList) {
        this.groupCode = "G-" + ++Group.codes;
        this.classroomTeacher = classroomTeacher;
        this.studentsList = studentsList;
        this.classJournal = new Journal();
    }

    public String getGroupCode() {
        return groupCode;
    }

    public void setGroupCode(String groupCode) {
        this.groupCode = groupCode;
    }

    public Teacher getClassroomTeacher() {
        return classroomTeacher;
    }

    public void setClassroomTeacher(Teacher classroomTeacher) {
        this.classroomTeacher = classroomTeacher;
    }

    public LinkedList<Student> getStudentsList() {
        return studentsList;
    }

    public Journal getClassJournal() {
        return classJournal;
    }

    public void outputStudentsList() {
        StringBuilder studentsOutputList = new StringBuilder();

        studentsOutputList.append(groupCode).append(" students list\n").
                append("Classroom teacher: ").
                append(classroomTeacher.getFullName()).append('\n');

        for(int i = 0; i < studentsList.size(); i++) {
            studentsOutputList.append(new Formatter().format("%3d) %50s\n",
                    i + 1,
                    studentsList.get(i).getFullName()));
        }

        System.out.print(studentsOutputList);
    }

    public class Journal implements Serializable {

        private Date creationDate;

        private LinkedList<Row> evaluationsLog;

        private LinkedList<Date> requestsLog;

        private Journal() {
            if(evaluationsLog == null) {
                this.evaluationsLog = new LinkedList<>();
                this.requestsLog = new LinkedList<>();
                this.creationDate = new Date();
            } else {
                this.requestsLog.add(new Date());
            }
        }

        public Date getCreationDate() {
            return creationDate;
        }

        public LinkedList<Row> getEvaluationsLog() {
            addEventIntoRequestLog();
            return evaluationsLog;
        }

        public void outputJournal() {
            StringBuilder journalList = new StringBuilder();
            System.out.print(outputJournal(journalList));
        }

        public String outputJournal(StringBuilder StringForAdding) {
            StringForAdding.append(groupCode).append(" journal\n");

            if(evaluationsLog.isEmpty()) {
                StringForAdding.append("Is empty yet..\n");
            } else {
                for (int i = 0; i < evaluationsLog.size(); i++) {
                    Row row = evaluationsLog.get(i);
                    StringForAdding.append(new Formatter().format("%3d) %30s | %20s | %5d | %5$tY/%5$tm/%5$te %5$tH:%5$tM:%5$tS.%5$tL\n",
                            i + 1,
                            row.getStudent().getFullName(),
                            row.getSubject().getName(),
                            row.getMark(),
                            row.getEventTime()));
                }
            }
            return StringForAdding.toString();
        }

        public void addRow(Row row) {
            this.evaluationsLog.add(row);
            addEventIntoRequestLog();
        }

        public void addRow(Student student, Subject subject, int mark) {
            Row row = new Row(student, subject, mark);
            evaluationsLog.add(row);
            addEventIntoRequestLog();
        }

        public void addRow(Student student, Subject subject, int mark, String eventDateString) {
            Date eventDate = null;
            try {
                eventDate = DateFormat.getInstance().parse(eventDateString);
            } catch (ParseException e) {
                e.printStackTrace();
            } finally {
                if(eventDate == null) {
                    eventDate = new Date();
                }
            }

            Row row = new Row(student, subject, mark, eventDate);
            evaluationsLog.add(row);
            addEventIntoRequestLog();
        }

        public void removeRowsByStudentName(Student student) {
            Predicate<Row> personPredicate = p -> p.getStudent().getFullName().equals(student.getFullName());
            evaluationsLog.removeIf(personPredicate);
            addEventIntoRequestLog();
        }

        public void removeRowByIndex(int indexOfrowForRemoving) {
            evaluationsLog.remove(indexOfrowForRemoving);
            addEventIntoRequestLog();
        }

        public LinkedList<Date> getRequestsLog() {
            return requestsLog;
        }

        private void addEventIntoRequestLog() {
            this.getRequestsLog().add(new Date());
        }

        public class Row implements Serializable {

            private Student student;

            private Subject subject;

            private int mark;

            private Date eventTime;

            private Row(Student student, Subject subject, int mark) {
                this.student = student;
                this.subject = subject;
                this.mark = mark;
                this.eventTime = new Date();
            }

            private Row(Student student, Subject subject, int mark, Date date) {
                this.student = student;
                this.subject = subject;
                this.mark = mark;
                this.eventTime = date;
            }

            public Student getStudent() {
                return student;
            }

            public Subject getSubject() {
                return subject;
            }

            public int getMark() {
                return mark;
            }

            public void setMark(int mark) {
                this.mark = mark;
            }

            public Date getEventTime() {
                return eventTime;
            }

            public void outputRow() {
                String rowOutput = new Formatter().format("Row is:\nStudent name: %30s\nSubject name: %20s\nGrade is: %5d\nEvent time is: %4$tY/%4$tm/%4$te %4$tH:%4$tM:%4$tS.%4$tL\n",
                        student.getFullName(),
                        subject.getName(),
                        mark,
                        eventTime).toString();
                System.out.print(rowOutput);
            }
        }
    }
}
