package ru.shem.data.structures;

import ru.shem.data.souls.Teacher;

import java.io.Serializable;

public class Subject implements Serializable {

    private String name;

    private Teacher teacher;

    public Subject(String name, Teacher teacher) {
        this.name = name;
        this.teacher = teacher;
    }

    public Subject(String name) {
        this.name = name;
        this.teacher = null;
    }

    public String getName() {
        return name;
    }

    public Teacher getTeacher() {
        return teacher;
    }

    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }
}
