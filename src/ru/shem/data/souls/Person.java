package ru.shem.data.souls;

import java.io.Serializable;

public interface Person extends Serializable {

    String getFirstName();

    void setFirstName(String firstName);

    String getLastName();

    void setLastName(String lastName);

    String getFullName();
}
