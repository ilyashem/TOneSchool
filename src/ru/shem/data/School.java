package ru.shem.data;

import ru.shem.data.souls.Student;
import ru.shem.data.structures.Group;
import ru.shem.data.structures.Subject;

import java.util.LinkedList;
import java.util.Scanner;

public class School {

    private final int NUMBER = 101;

    private LinkedList<Group> groupList;

    private LinkedList<Subject> subjectList;

    public School(LinkedList<Group> groupList, LinkedList<Subject> subjectList) {
        this.groupList = groupList;
        this.subjectList = subjectList;
    }

    public LinkedList<Group> getGroupList() {
        return groupList;
    }

    public LinkedList<Subject> getSubjectList() {
        return subjectList;
    }

    public int getNumber() {
        return NUMBER;
    }

    public void fillJournals() {
        groupList.forEach(group -> {
            group.getStudentsList().forEach(student -> {
                int randomSubjectIndex = (int) (Math.random() * (subjectList.size()));
                int mark = 2 + (int) (Math.random() * 4);
                group.getClassJournal().addRow(student,
                        subjectList.get(randomSubjectIndex),
                        mark);
            });
        });
    }

    private void outputGroupsList() {
        StringBuilder groupsOutputList = new StringBuilder();

        groupsOutputList.append("Group list:\n");
        groupList.forEach(group -> {
            groupsOutputList.append(group.getGroupCode()).append('\n');
        });

        System.out.print(groupsOutputList);
    }

    private void outputSubjectsList() {
        StringBuilder subjectOutputList = new StringBuilder();

        subjectOutputList.append("Subject list:\n");
        for(int i = 0; i < subjectList.size(); i++) {
            Subject subject = subjectList.get(i);

            subjectOutputList.append(i + 1).append(") ").
                    append(subject.getName()).
                    append(" (").
                    append(subject.getTeacher().getFullName()).
                    append(")\n");
        }

        System.out.print(subjectOutputList);
    }

    public void riseUpAllLists() {
        StringBuilder allListReport = new StringBuilder();

        allListReport.append("School #").append(NUMBER).append('\n').
                append("Teaching subjects:\n");
        subjectList.forEach(subject -> {
            allListReport.append(subject.getName()).
                    append(", teacher: ").
                    append(subject.getTeacher().getFullName()).append('\n');
        });
        allListReport.append('\n');

        groupList.forEach(group -> {
            allListReport.append("Group ID: ").append(group.getGroupCode()).append('\n').
                    append("Classroom teacher: ").
                    append(group.getClassroomTeacher().getFullName()).append("\n\n").
                    append("Students:\n");

            group.getStudentsList().forEach(student -> {
                allListReport.append(student.getFullName()).append('\n');
            });

            allListReport.append("\n---------------------------------------------------------\n");
            allListReport.append(group.getClassJournal().outputJournal(new StringBuilder()));
            allListReport.append("\n=========================================================\n\n");
        });

        System.out.print(allListReport);
    }

    public void journalActions() {
        Scanner scanner = new Scanner(System.in);

        outputGroupsList();

        System.out.print("\nChoice a group (Enter number without \"G-\"): ");
        Group chosenGroup = groupList.get(scanner.nextInt() - 1);

        chosenGroup.getClassJournal().outputJournal();

        System.out.print("\nChoice the action (1 – add row, 2 – edit row, 3 – delete row): ");

        switch(scanner.nextInt()) {
            case 1:
                startLesson(chosenGroup);
                break;
            case 2:
                System.out.print("\nChoice the row by number: ");
                journalEdit(chosenGroup,scanner.nextInt() - 1);
                break;
            case 3:
                System.out.print("\nChoice the row for deleting by number: ");
                chosenGroup.getClassJournal().removeRowByIndex(scanner.nextInt() - 1);
                break;
            default:
                break;
        }
    }

    private void startLesson(Group chosenGroup) {
        Scanner scanner = new Scanner(System.in);

        chosenGroup.outputStudentsList();
        System.out.print("\nChoice a student (Enter the number from list): ");
        Student chosenStudent = chosenGroup.getStudentsList().get(scanner.nextInt() - 1);

        outputSubjectsList();
        System.out.print("\nChoice a subject (Enter the number from list): ");
        Subject chosenSubject = subjectList.get(scanner.nextInt() - 1);

        System.out.print(new StringBuilder().append(chosenGroup.getGroupCode()).
                append(", student – ").append(chosenStudent.getFullName()).
                append(" has a ").append(chosenSubject.getName()).append(" score of "));
        int score = scanner.nextInt();

        chosenGroup.getClassJournal().addRow(chosenStudent, chosenSubject, score);
        chosenGroup.getClassJournal().outputJournal();

        scanner.nextLine();
        scanner.skip("\n");
    }

    private void journalEdit(Group editingGroup, int numberOfJournalRow) {
        Scanner scanner = new Scanner(System.in);

        editingGroup.getClassJournal().getEvaluationsLog().get(numberOfJournalRow).outputRow();
        System.out.print("\nYou can change the grade. New grade is: ");
        editingGroup.getClassJournal().getEvaluationsLog().get(numberOfJournalRow).setMark(scanner.nextInt());
    }
}
