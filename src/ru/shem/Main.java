package ru.shem;

import ru.shem.model.SchoolControls;

import java.util.Scanner;

public class Main {

    private static boolean isDataFilling = false;
    private static boolean isSaveData = true;

    private final static SchoolControls schoolControls = new SchoolControls();

    public static void main(String[] args) {
        for(String argument : args) {
            switch(argument) {
                case "-fill":
                    isDataFilling = true;
                    break;
                case "-not_save":
                    isSaveData = false;
                    break;
                default:
                    break;
            }
        }

        if(isDataFilling) {
            schoolControls.getStaticSchool().fillJournals();
        }

        menu_init();

        if(isSaveData) {
            schoolControls.save();
        }
    }

    private static void menu_init() {
        StringBuilder menu_list = new StringBuilder();

        menu_list.append("School #").append(schoolControls.getStaticSchool().getNumber()).append('\n').
                append("Welcome! Have a nice day!\n\n").
                append("1. Get full report\n").
                append("2. Journal actions\n").
                append("\n0. Exit terminal\n\n").
                append("Enter command code: ");

        while(true) {
            System.out.print(menu_list);

            Scanner scanner = new Scanner(System.in);
            int command = scanner.nextInt();

            switch(command) {
                case 1:
                    schoolControls.getStaticSchool().riseUpAllLists();
                    scanner.nextLine();
                    scanner.skip("\n");
                    break;
                case 2:
                    schoolControls.getStaticSchool().journalActions();
                    break;
                case 0:
                    return;
                default:
                    System.out.println("Wrong code.. try again.");
                    break;
            }
        }
    }
}
