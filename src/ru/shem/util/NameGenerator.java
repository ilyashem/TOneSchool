package ru.shem.util;

import java.util.Random;

public class NameGenerator {

    private static String[] firstBeginning = { "Kr", "Ca", "Ra", "Mrok", "Cru",
            "Ray", "Bre", "Zed", "Drak", "Mor", "Jag", "Mer", "Jar", "Mjol",
            "Zork", "Mad", "Cry", "Zur", "Creo", "Azak", "Azur", "Rei", "Cro",
            "Mar", "Luk" };

    private static String[] firstMiddle = { "air", "ir", "mi", "sor", "mee", "clo",
            "red", "cra", "ark", "arc", "miri", "lori", "cres", "mur", "zer",
            "marac", "zoir", "slamar", "salmar", "urak" };

    private static String[] firstEnd = { "d", "ed", "ark", "arc", "es", "er", "der",
            "tron", "med", "ure", "zur", "cred", "mur" };

    private static String[] lastBeginning = { "Rk", "Ac", "Ar", "Korm", "Urc",
            "Yar", "Erb", "Dez", "Kard", "Rom", "Gaj", "Rem", "Raj", "Lojm",
            "Kroz", "Dam", "Yrc", "Ruz", "Oerc", "Kaza", "Ruza", "Ier", "Ore",
            "Ram", "Kul" };

    private static String[] lastMiddle = { "ria", "ri", "im", "ros", "eem", "olc",
            "der", "arc", "kra", "rac", "irim", "irol", "srec", "rum", "rez",
            "caram", "rioz", "ramals", "ramlas", "karu" };

    private static String[] lastEnd = { "d", "ed", "ark", "arc", "es", "er", "der",
            "tron", "med", "ure", "zur", "cred", "mur" };

    private static Random randomIndex = new Random();

    private static String generateFirstName() {
        return firstBeginning[randomIndex.nextInt(firstBeginning.length)] +
                firstMiddle[randomIndex.nextInt(firstMiddle.length)]+
                firstEnd[randomIndex.nextInt(firstEnd.length)];
    }

    private static String generateLastName() {
        return lastBeginning[randomIndex.nextInt(firstBeginning.length)] +
                lastMiddle[randomIndex.nextInt(firstMiddle.length)]+
                lastEnd[randomIndex.nextInt(firstEnd.length)];
    }

    private static String generateFullName() {
        return generateFirstName() + ' ' + generateLastName();
    }

    public static String[] generateNamesArray(int countOfNames) {
        String[] names = new String[countOfNames];

        for(int i = 0; i < names.length; i++) {
            names[i] = generateFullName();
        }

        return names;
    }
}
